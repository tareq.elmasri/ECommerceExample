#!/bin/bash

clear

# Functions ==============================================

# return 1 if global command line program installed, else 0
# example
# echo "node: $(program_is_installed node)"
function program_is_installed {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  echo "$return_"
}

# return 1 if local npm package is installed at ./node_modules, else 0
# example
# echo "gruntacular : $(npm_package_is_installed gruntacular)"
function npm_package_is_installed {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  ls node_modules | grep $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  echo "$return_"
}

# display a message in red with a cross by it
# example
# echo echo_fail "No"
function echo_fail {
  # echo first argument in red
  printf "\e[31m ${1}"
  # reset colours back to normal
  echo "\033[0m"
}

# display a message in green with a tick by it
# example
# echo echo_fail "Yes"
function echo_pass {
  # echo first argument in green
  printf "\e[32m ${1}"
  # reset colours back to normal
  echo "\033[0m"
}

function echo_pass {
  # echo first argument in green
  printf "\e[32m ${1}"
  # reset colours back to normal
  echo "\033[0m"
}

# ============================================== Functions
if [ program_is_installed node == 1 ]; then
    echo_pass "Node.JS is installed ✔";
    npm install -g bower node-sass gulp yo && npm install && bower install;
    if [ npm_package_is_installed node == 1 ]; then
        
    else

    fi
else
    echo_fail "Node.JS isn't installed, please install: https://nodejs.org/en/"
fi