/* global require */
//|**
//|
//| Minimal
//|
//| This file is the main application file
//|
//| .--------------------------------------------------------------.
//| | NAMING CONVENTIONS:                                          |
//| |--------------------------------------------------------------|
//| | Singleton-literals and prototype objects | PascalCase        |
//| |--------------------------------------------------------------|
//| | Functions and public variables           | camelCase         |
//| |--------------------------------------------------------------|
//| | Global variables and constants           | UPPERCASE         |
//| |--------------------------------------------------------------|
//| | Private variables                        | _underscorePrefix |
//| '--------------------------------------------------------------'
//|
//| Comment syntax for the entire project follows JSDoc:
//| - http://code.google.com/p/jsdoc-toolkit/wiki/TagReference
//|
//'*/
require.config({
  deps: ['application'],
  waitSeconds: 45,
  paths: {
    html5shiv: 'vendor/html5shiv/dist/html5shiv',
    easing: 'vendor/fullpage.js/vendors/jquery.easings.min',
    underscore: 'vendor/underscore/underscore',
    modernizr: 'vendor/modernizr/modernizr',
    backbone: 'vendor/backbone/backbone',
    requirejs: 'vendor/requirejs/require',
    mousewheel: 'vendor/jquery-mousewheel/jquery.mousewheel',
    jquery: 'vendor/jquery/dist/jquery',
    skrollr: 'vendor/skrollr/src/skrollr',
    bootstrap: 'vendor/bootstrap-sass-official/assets/javascripts/bootstrap.min',
    bootstrap_dropdown: 'lib/bootstrap-hover-dropdown'
  },
  shim: {
    easing: {
      deps: ['jquery']
    },
    bootstrap: {
      deps: ['jquery']
    },
    bootstrap_dropdown: {
      deps: ['bootstrap']
    },
    mousewheel: {
      deps: ['jquery']
    },
    backbone: {
      deps: ['underscore']
    }
  }
});
