/* global define */
define([
  'jquery',
  'backbone',
  'skrollr'
], function($, Backbone, skrollr) {
  'use strict';
  var Sample = Backbone.View.extend({
    initialize: function() {
      this.render();
    },
    render: function() {
        var s = skrollr.init();
    },
    events: {
    },
  });
  return Sample;
});
